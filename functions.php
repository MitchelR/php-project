<?php
include("connection.php");

function mysqli_query_params($link, $sql, $params, $types = NULL)
{
    $statement = $link->prepare($sql);
    $types = str_repeat('s', count($params));
    $statement->bind_param($types, ...$params);
    $statement->execute();
    return $statement;
}
?>
<?php
session_start();
include("connection.php");
include("functions.php");
require_once('vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet; 
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$infoMessage = "";
$errorMessage = 0;
$tblPaging = "";
$tblresult = "";
$pagetitle = "System";
$tblUsers = true;
$searchtbl = false;
$excel = false;
$currentpage = 1;
$disabled = "disabled=true";
$rowsperpage = 10;
$offset = "";
$Totalpages = "";
$countresults = "";

if(!isset($_SESSION['selects'])) {
    $_SESSION['selects']['naam'] = $_POST['InpName'];
    $_SESSION['selects']['adres'] = $_POST['InpAdres'];
    $_SESSION['selects']['woonplaats'] = $_POST['InpWoonplaats'];
}

if($_SERVER['REQUEST_METHOD'] == "POST") {

    if(isset($_POST['clear'])) {
        unset($_SESSION['selects']);
    }

    if(isset($_POST['search'])) {
        $_SESSION['selects']['naam'] = $_POST['InpName'];
        $_SESSION['selects']['adres'] = $_POST['InpAdres'];
        $_SESSION['selects']['woonplaats'] = $_POST['InpWoonplaats'];
        $searchtbl = true;
        $tblUsers = false;
    }

    if(isset($_POST['excel'])) {
        if(!empty($_SESSION['selects']['naam']) || !empty($_SESSION['selects']['adres']) || !empty($_SESSION['selects']['woonplaats'])) {
            $searchtbl = true;
            $tblUsers = false;
        }

        $xls = new Spreadsheet();
    
        // Retrieve the current active worksheet 
        $ws = $xls->getActiveSheet();
        $ws->setTitle("System"); 
    
        // Set the value of cell A1 
        $ws->setCellValue('A1', 'Naam'); 
        $ws->setCellValue('B1', 'Adres');
        $ws->setCellValue('C1', 'Woonplaats');
        $ws->getDefaultColumnDimension()->setWidth(25);
        $styleArrayFirstRow = ['font' => ['bold' => true,]];
    
        //Retrieve Highest Column (e.g AE)
        $highestColumn = $ws->getHighestColumn();
    
        //set first row bold
        $ws->getStyle('A1:' . $highestColumn . '1' )->applyFromArray($styleArrayFirstRow);
        $xls->getSheetByName('System');
        $excel = true;
    }
    
    if(isset($_POST['Currentpage'])) {

        if(!empty($_SESSION['selects']['naam']) || !empty($_SESSION['selects']['adres']) || !empty($_SESSION['selects']['woonplaats'])) {
            $searchtbl = true;
            $tblUsers = false;
        }

        if(isset($_POST['NextBtn'])) {
            $currentpage++;
        }

        if(isset($_POST['PrevBtn'])) {
            $currentpage -1;
        }
    }
}

if($searchtbl) {
    $sql = "SELECT * FROM `users`"; 

    $wheres = array();
    $params = array();

    $selects = $_SESSION['selects'];
    $name = $selects['naam'];
    $adres = $selects['adres'];
    $woonplaats = $selects['woonplaats'];
    
    if($name != "") {
        $wheres[] = "Naam like ?";
        $params[] = "%".$name."%";
    }

    if($adres != "") {
        $wheres[] = "Adres like ?";
        $params[] = "%".$adres."%";
    }

    if($woonplaats != "") {
        $wheres[] = "Woonplaats like ?";
        $params[] = "%".$woonplaats."%";
    }
    
    //htmlspecialchars en entities
    
    $sql .= " WHERE ".implode(" AND ",$wheres);
    
    if(count($wheres) > 0) {
    $data = mysqli_query_params($link, $sql, $params, "si")->get_result()->fetch_all();
    $countresults = count($data);

    $Totalpages = ceil($countresults / $rowsperpage);
    $offset = ($currentpage - 1) * $rowsperpage;

    $sql .= " ORDER BY ID";
    $sql .= " LIMIT $rowsperpage OFFSET $offset";
    
    $result = mysqli_query_params($link, $sql, $params, "si")->get_result();
    }
    else {
        $infoMessage .= "Er moet minimaal één veld ingevuld worden.";
    }

}

if($tblUsers) {
    $count = "SELECT COUNT(*) AS total FROM users";
    $sql = "SELECT * FROM `users`"; 
    
    $countresult = mysqli_query($link, $count);
    $row = mysqli_fetch_array($countresult);
    $countresults = $row['total'];

    $Totalpages = ceil($countresults / $rowsperpage);
    $offset = ($currentpage - 1) * $rowsperpage;

    $sql .= " ORDER BY ID";
    $sql .= " LIMIT $rowsperpage OFFSET $offset";

    $result = mysqli_query($link, $sql);    
    
}

if(isset($result)) {

    $tblresult .= "
    <form><table id='maintable' width='100%'>
        <th>Naam</th>
        <th>Adres</th>
        <th>Woonplaats</th>
    ";
    $rowcount = 2;
foreach($result as $rows){
    if($excel) {
    $ws->setCellValue('A'.$rowcount, $rows['Naam']); 
    $ws->setCellValue('B'.$rowcount, $rows['Adres']);
    $ws->setCellValue('C'.$rowcount, $rows['Woonplaats']);
    $rowcount++;
    }

    $tblresult .= "
        <tr id='".$rows['ID']."'>
            <td><input name='name' type='text' class='form-control' $disabled value='".$rows['Naam']."' /></td>
            <td><input name='adres' type='text' class='form-control' $disabled value='".$rows['Adres']."' /></td>
            <td><input name='woonplaats' type='text' class='form-control' $disabled value='".$rows['Woonplaats']."' /></td>
            <td><button name='edit' type='button' class='btn editrow' value='".$rows['ID']."'>Edit</button></td>
            <td><button name='delete' type='button' class='btn deleterow' value='".$rows['ID']."'>Delete</button></td>
        </tr>
    ";
}
$tblresult .= "</table></form>";
}
if($excel) {
$writer = new Xlsx($xls);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="System.xlsx"');
$writer->save('php://output');
exit;
}

$tblPaging .= "<form action='' method='post'><table>
    <tr>
        <td><input name='PrevBtn' type='submit' value='Previous page' /></td>
        <td><input name='Currentpage' type='hidden' value='".$currentpage."' /></td>
        <td>Page: ".$currentpage." of ".$Totalpages."</td>
        <td><input name='NextBtn' type='submit' value='Next page' /></td>
</table></form>";

    if($countresults > $rowsperpage) {
        $tblresult .= $tblPaging;
    }

?>

<html>
    <head>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-3.1.1.min.js"></script>
    </head>
    <body style="margin:auto; width:75%;">
        <div class="h1 text-center">
            <?php print $pagetitle; ?>
        </div>
        <form action="" method="post">
        <div class="row">
            <div class="col-lg-4">
                <label class="d-print-none">Naam</label>
                <input name="InpName" type="text" class="form-control d-print-none" value="" />
            </div>
            <div class="col-lg-4">
                <label class="d-print-none">Adres</label>
                <input name="InpAdres" type="text" class="form-control d-print-none" value="" />
            </div>
            <div class="col-lg-4">
                <label class="d-print-none">Woonplaats</label>
                <input name="InpWoonplaats" type="text" class="form-control d-print-none" value="" />
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <input name="search" type="submit" value="Search" class="searchrows d-print-none" />
                <input name="clear" type="submit" value="Clear" class="d-print-none" />
                <input id="NewRow" type="submit" value="New Field" class="d-print-none" />
                <input type="submit" value="Print" class="d-print-none" onClick="window.print(); return false;" />
                <input name="excel" type="submit" value="Excel" class="d-print-none" />
            </div>
            <div class="col-lg-4"></div>
            <div class="col-lg-4"></div>
        </div>
        </form>
        <?php
        if($infoMessage != "") {
        ?>
        <div>
        <?php
        print $infoMessage;
        $tblresult = false;
        ?>
        </div>
        <?php
        }
        else {
        ?>
        <div id="container">
            <?php print $tblresult; ?>
        </div>
        <?php
        }
        ?>

        <script>
        $(document).ready(function(){

            $('.editrow').on('click', function(){
            var id = $(this).closest("tr").attr("id");
            var name = $("#" + id).find('input[name=name]').val();
            var adres = $("#" + id).find('input[name=adres]').val();
            var woonplaats = $("#" + id).find('input[name=woonplaats]').val();

            localStorage.setItem('naam', name);
            localStorage.setItem('adres', adres);
            localStorage.setItem('woonplaats', woonplaats);

            $(this).closest('tr').find('input').prop('disabled', false);
            
            $(this).closest('.editrow').css("display", "none").after("<button name='save' type='button' class='btn saverow'>Save</button>");
            $(this).closest('tr').find('.deleterow').css("display", "none").after("<button name='undo' type='button' class='btn undorow'>Undo</button>");
            });

            $('.deleterow').on('click', function(){
                var id = $(this).closest("tr").attr("id");
                if(confirm("Weet u het zeker?")) {
                    $.ajax({
                        type: "POST",
                        url: "index_ajax.php",
                        data: { action: "getdelete", id: id },
                        success: function () {
                            $("#" + id).remove();
                        }
                    })
                }
            })

            $(document).on('click',".saverow", function(){
                var id = $(this).closest("tr").attr("id");
                var err = "";
                
                var name = $("#" + id).find('input[name=name]').val();
                if(name == 0) {
                    err += "Field name is empty." + "\n";
                    $(this).closest('tr').find('input[name=name]').css("border", "1px solid red");
                }
                else {
                    $(this).closest('tr').find('input[name=name]').css("border", "1px solid #ced4da");
                }

                var adres = $("#" + id).find('input[name=adres]').val();
                if(adres == 0){
                    err += "Field adres is empty." + "\n";
                    $(this).closest('tr').find('input[name=adres]').css("border", "1px solid red");    
                }
                else {
                    $(this).closest('tr').find('input[name=adres]').css("border", "1px solid #ced4da");
                }

                var woonplaats = $("#" + id).find('input[name=woonplaats]').val();
                if(woonplaats == 0) {
                    err += "Field woonplaats is empty.";
                    $(this).closest('tr').find('input[name=woonplaats]').css("border", "1px solid red");
                }
                else {
                    $(this).closest('tr').find('input[name=woonplaats]').css("border", "1px solid #ced4da");
                }

                if(err != 0) {
                    err = "Please check the following:" + '\n' + err;
                    alert(err);
                }
                else {
                    $("#" + id).find('.saverow').css("display", "none");
                    $("#" + id).find('.undorow').css("display", "none");
                    $(this).closest('tr').find('.editrow').css("display", "block");
                    $(this).closest('tr').find('.deleterow').css("display", "block");
                    $(this).closest('tr').find('input').prop('disabled', true);
                    $.ajax({
                        type: "POST",
                        url: "index_ajax.php",
                        data: { action: "getsave", id: id, name:name, adres:adres, woonplaats:woonplaats },
                        success: function (message) {
                            alert(message);
                        }
                    })
                }

            });

            $('#NewRow').on('click', function(){
                var tr = (
                "<tr id='0'>" +
                    "<td><input name='name' type='text' class='form-control' /></td>" +
                    "<td><input name='adres' type='text' class='form-control' /></td>" +
                    "<td><input name='woonplaats' type='text' class='form-control' /></td>" +
                    "<td><button name='save' type='button' class='btn saverow'>Save</button></td>" +
                '</tr>'
                );
                $('#maintable').append(tr);
                return false;
            });

            $(document).on('click',".undorow", function(){
                
                var trid = $(this).closest('tr').attr("id");
                var name = localStorage.getItem('naam');
                var adres = localStorage.getItem('adres');
                var woonplaats = localStorage.getItem('woonplaats');

                if(confirm("Do you want to restore your values?")) {

                    $(this).closest('tr').find('input').prop('disabled', true);

                    $("#" + trid).find('input[name=name]').val(name);
                    $("#" + trid).find('input[name=adres]').val(adres);
                    $("#" + trid).find('input[name=woonplaats]').val(woonplaats);
                    $("#" + trid).find('.saverow').css("display", "none");
                    $("#" + trid).find('.undorow').css("display", "none");
                    $(this).closest('tr').find('.editrow').css("display", "block");
                    $(this).closest('tr').find('.deleterow').css("display", "block");
                }
            });

        });
        </script>

    </body>
</html>